<?php
/**
* Plugin Name: Basic Plugin
* Description: Etiam dictum mattis ante vel suscipit. Fusce ac turpis nunc.
* Plugin URI: http://wp_training_local.com
* Author: TamNC
* Author URI: http://wp_training_local.com
* Version: 1.0
* License: GPLv2
*/

function tamnc_remove_dashboard_widget() {
	remove_meta_box('dashboard_primary', 'dashboard', 'side');
}
add_action('wp_dashboard_setup', 'tamnc_remove_dashboard_widget');

function tamnc_add_google_analytics_link() {
	global $wp_admin_bar;
	$wp_admin_bar->add_menu(array(
		'id' => 'google_analytics',
		'title' => 'Google Analytics',
		'href' => 'http://google.com/analytics',
	));
}
add_action('wp_before_admin_bar_render', 'tamnc_add_google_analytics_link');