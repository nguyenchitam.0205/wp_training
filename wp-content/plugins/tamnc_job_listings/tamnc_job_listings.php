<?php
/**
 * Plugin Name: Job Listings
 * Description: Etiam dictum mattis ante vel suscipit. Fusce ac turpis nunc.
 * Plugin URI: http://wp_training_local.com
 * Author: TamNC
 * Author URI: http://wp_training_local.com
 * Version: 1.0
 * License: GPLv2
 */

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

// Create Job post type and Location taxonomy in another file.
require_once(plugin_dir_path(__FILE__) . 'tamnc_job_cpt.php');
require_once(plugin_dir_path(__FILE__) . 'tamnc_job_render_admin.php');
require_once(plugin_dir_path(__FILE__) . 'tamnc_job_fields.php');
require_once(plugin_dir_path(__FILE__) . 'tamnc_job_shortcode.php');
require_once(plugin_dir_path(__FILE__) . 'tamnc_job_widget.php');

function tamnc_admin_enqueue_scripts()
{
    global $pagenow, $typenow;

    if ($typenow == 'job') {
        wp_enqueue_style('tamnc_job_listings_css', plugins_url('css/tamnc_job_listings.css', __FILE__));
    }

    if (($pagenow == 'post.php' OR $pagenow == 'post-new.php') OR $typenow == 'job') {

        wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
        wp_enqueue_script('tamnc_job_listings_js', plugins_url('js/tamnc_job_listings.js', __FILE__), array('jquery', 'jquery-ui-datepicker'), '20150204', true);
        wp_enqueue_script('tamnc_job_quicktags_js', plugins_url('js/tamnc_job_quicktags.js', __FILE__), array('quicktags'), '20150206', true);
    }

    if ($pagenow == 'edit.php' AND $typenow == 'job') {
        wp_enqueue_script('tamnc_job_reorder_js', plugins_url('js/tamnc_job_reorder.js', __FILE__), array('jquery', 'jquery-ui-sortable'), '20150626', true);
        wp_localize_script('tamnc_job_reorder_js', 'WP_JOB_REORDER', array(
            'security' => wp_create_nonce('job_reorder_nonce'),
            'successMsg' => 'Job sort order has been saved.',
            'errorMsg' => 'There was an error saving the job sort order.',
        ));
    }
}

add_action('admin_enqueue_scripts', 'tamnc_admin_enqueue_scripts');


function tamnc_add_custom_quicktags()
{
    wp_enqueue_script('tamnc_job_quicktags_js', plugins_url('js/tamnc_job_quicktags.js', __FILE__), array('quicktags'), '20150206', true);
}

add_action('admin_print_scripts', 'tamnc_add_custom_quicktags');
