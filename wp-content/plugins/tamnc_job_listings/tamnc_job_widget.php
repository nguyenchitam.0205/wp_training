<?php

class TamNCJobWidget extends WP_Widget
{
    // Widget constructor.
    public function __construct()
    {
        parent::__construct(
            'text_job_widget',
            __('Text Job Widget', 'textjobwidgetdomain'),
            array(
                'classname' => 'text_job_widget',
                'description' => __('A basic text widget to demo on creating your own widgets.', 'textjobwidgetdomain')
            )
        );

        load_plugin_textdomain('textjobwidgetdomain', false, basename(dirname(__FILE__)) . '/languages');
    }

    // Output the content of the widget.
    public function widget($args, $instance)
    {
        extract($args);

        $title = apply_filters('widget_title', $instance['title']);
        $message = $instance['message'];

        echo $before_widget;

        if ($title) {
            echo $before_title . $title . $after_title;
        }
        echo $message;

        echo $after_widget;
    }

    // Creates the back-end form.
    public function form($instance)
    {
        $title = isset($instance['title']) ? esc_attr($instance['title']) : '';
        $message = isset($instance['message']) ? esc_attr($instance['message']) : '';
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>"/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('message'); ?>"><?php _e('Simple Message'); ?></label>
            <textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id('message'); ?>"
                      name="<?php echo $this->get_field_name('message'); ?>"><?php echo $message; ?></textarea>
        </p>
        <?php
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;

        $instance['title'] = strip_tags($new_instance['title']);
        $instance['message'] = strip_tags($new_instance['message']);

        return $instance;
    }

}

add_action('widgets_init', function () {
    register_widget('TamNCJobWidget');
});