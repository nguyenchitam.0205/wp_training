<?php

function add_custom_metabox()
{
    add_meta_box(
        'tamnc_meta_box',
        __('Job listing'),
        'tamnc_meta_box_callback',
        'job',
        'advanced',
        'high'
    );
}

add_action('add_meta_boxes', 'add_custom_metabox');

function tamnc_meta_box_callback($post)
{
    wp_nonce_field(basename(__FILE__), 'meta_box_nonce_field');
    $post_data = get_post_meta($post->ID);
    $txt_job_id = isset($post_data['txt_job_id']) ? $post_data['txt_job_id'][0] : '';
    $txt_date_listed = isset($post_data['txt_date_listed']) ? $post_data['txt_date_listed'][0] : '';
    $txt_application_deadline = isset($post_data['txt_application_deadline']) ? $post_data['txt_application_deadline'][0] : '';
    $txa_minimum_requirements = isset($post_data['txa_minimum_requirements']) ? $post_data['txa_minimum_requirements'][0] : '';
    ?>
    <div>
        <div class="meta-row">
            <div class="meta-th">
                <label for="lbl_job_id"><?php esc_html_e('Job ID'); ?></label>
            </div>
            <div class="meta-td">
                <input type="text" name="txt_job_id" id="txt_job_id" value="<?php print $txt_job_id; ?>"/>
            </div>
        </div>
        <div class="meta-row">
            <div class="meta-th">
                <label for="lbl_date_listed"><?php esc_html_e('Date Listed'); ?></label>
            </div>
            <div class="meta-td">
                <input type="text" class="datepicker" name="txt_date_listed" id="txt_date_listed" value="<?php print $txt_date_listed; ?>"/>
            </div>
        </div>
        <div class="meta-row">
            <div class="meta-th">
                <label for="lbl_application_deadline"><?php esc_html_e('Application Deadline'); ?></label>
            </div>
            <div class="meta-td">
                <input type="text" class="datepicker" name="txt_application_deadline" id="txt_application_deadline" value="<?php print $txt_application_deadline; ?>"/>
            </div>
        </div>
        <div class="meta">
            <div class="meta-th">
                <span><?php esc_html_e('Principle Duties'); ?></span>
            </div>
        </div>
        <div class="meta-editor">
            <?php
            $editor_id = 'principle_duties';
            $content = get_post_meta($post->ID, $editor_id, true);
            $settings = array(
                'media_buttons' => false,
                'textarea_rows' => 5,
            );
            wp_editor($content, $editor_id, $settings);
            ?>
        </div>
        <div class="meta-row">
            <div class="meta-th">
                <label for="lbl_minimum_requirements"><?php esc_html_e('Minimum Requirements'); ?></label>
            </div>
            <div class="meta-td">
                <textarea name="txa_minimum_requirements" id="txa_minimum_requirements" class="custom-textarea"><?php print $txa_minimum_requirements; ?></textarea>
            </div>
        </div>
        <div class="meta-row">
            <div class="meta-th">
                <label for="lbl_relocation_assistance"><?php esc_html_e('Relocation Assistance'); ?></label>
            </div>
            <div class="meta-td">
                <select name="slt_relocation_assistance" id="slt_relocation_assistance">
                    <option value="Yes" <?php if ( ! empty ( $post_data['slt_relocation_assistance'] ) ) selected( $post_data['slt_relocation_assistance'][0], 'Yes' ); ?>><?php _e( 'Yes', 'wp-job-listing' )?></option>
                    <option value="No" <?php if ( ! empty ( $post_data['slt_relocation_assistance'] ) ) selected( $post_data['slt_relocation_assistance'][0], 'No' ); ?>><?php _e( 'No', 'wp-job-listing' )?></option>
                </select>
            </div>
        </div>
    </div>
    <?php
}
function tamnc_meta_box_save($post_id) {
    // Check save status.
    $is_autosave = wp_is_post_autosave($post_id);
    $is_revision = wp_is_post_revision($post_id);
    $is_valid_nonce = (isset($_POST['meta_box_nonce_field']) AND wp_verify_nonce($_POST['meta_box_nonce_field'], basename( __FILE__ ))) ? true : false;

    if ($is_autosave OR $is_revision OR !$is_valid_nonce) {
        return;
    }

    if (isset($_POST['txt_job_id'])) {
        update_post_meta($post_id, 'txt_job_id', $_POST['txt_job_id']);
    }

    if (isset($_POST['txt_date_listed'])) {
        update_post_meta($post_id, 'txt_date_listed', $_POST['txt_date_listed']);
    }

    if (isset($_POST['txt_application_deadline'])) {
        update_post_meta($post_id, 'txt_application_deadline', $_POST['txt_application_deadline']);
    }

    if (isset($_POST['principle_duties'])) {
        update_post_meta($post_id, 'principle_duties', $_POST['principle_duties']);
    }

    if (isset($_POST['txa_minimum_requirements'])) {
        update_post_meta($post_id, 'txa_minimum_requirements', $_POST['txa_minimum_requirements']);
    }

    if ( isset( $_POST[ 'slt_relocation_assistance' ] ) ) {
        update_post_meta( $post_id, 'slt_relocation_assistance', sanitize_text_field( $_POST[ 'slt_relocation_assistance' ] ) );
    }

}
add_action('save_post', 'tamnc_meta_box_save');