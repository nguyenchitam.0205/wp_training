jQuery(document).ready(function ($) {
    var joblist = $('.custom-type-list');
    var loadingAnimation = $('.loading-animation');
    var pageTitle = $('#job-sort h2');

    joblist.sortable({
        update: function (event, ui) {
            loadingAnimation.show();

            $.ajax({
                url: ajaxurl,
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'save_job_sort_order',
                    order: joblist.sortable('toArray'),
                    security: WP_JOB_REORDER.security,
                },
                success: function (res) {
                    $('.message').remove();
                    loadingAnimation.hide();
                    if (res.success == true) {
                        pageTitle.after('<div class="updated message">' + WP_JOB_REORDER.successMsg + '</div>');
                    }
                },
                error: function () {
                    $('.message').remove();
                    loadingAnimation.hide();
                    pageTitle.after('<div class="error message">' + WP_JOB_REORDER.errorMsg + '</div>');
                }
            });
        }
    });
});