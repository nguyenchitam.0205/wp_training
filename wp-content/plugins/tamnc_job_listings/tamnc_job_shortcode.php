<?php

function tamnc_shortcode_basic($attrs = array(), $content = NULL)
{
    // shortcode_atts() ==> Combine user attributes with known attributes and fill in defaults when needed.
    $attrs = shortcode_atts(
        array(
            'title' => 'Default Title',
            'url' => 'http://google.com',
        ), $attrs
    );
    return 'Less Code Less Bug ' . $attrs['title'] . ' ' . $attrs['url'];
}

add_shortcode('shortcode_basic', 'tamnc_shortcode_basic');

function tamnc_shortcode_location_list($attrs = array(), $content = NULL)
{
    $attrs = shortcode_atts(
        array(
            'title' => 'Current jobs opening in'
        ), $attrs
    );

    $locations = get_terms('location');
    $result = '';
    if (!empty($locations) AND !is_wp_error($locations)) {
        $result = '<div id=job-location-list>';
        $result .= '<h4>' . esc_html__($attrs['title']) . '</h4><ul>';
        foreach ($locations as $item) {
            $result .= '<li class="job-location">';
            $result .= '<a href="' . esc_url(get_term_link($item)) . '">' . esc_html__($item->name) . '</a>';
            $result .= '</li>';
        }
        $result .= '</div></ul>';
    }
    return $result;
}

add_shortcode('shortcode_location_list', 'tamnc_shortcode_location_list');

function tamnc_shortcode_job_by_location($attrs = array(), $content)
{
    if (isset($attrs['location']) AND !$attrs['location']) {
        return '<p class="job-error">You must provide a location for this shortcode to work.</p>';
    }

    $attrs = shortcode_atts(array(
        'title' => 'Current Job Openings in',
        'count' => 2,
        'location' => 'sai-gon',
        'pagination' => 'on'
    ), $attrs);

    $pagination = $attrs[ 'pagination' ]  == 'on' ? false : true;
    $paged = get_query_var('paged', 1);

    $args = array(
        'post_type' => 'job',
        'post_status' => 'publish',
        'no_found_rows' => $pagination, //pagination won’t work when you pass “no_found_rows=1”
        'posts_per_page' => $attrs['count'],
        'paged' => $paged,
        'tax_query' => array(
            array(
                'taxonomy' => 'location',
                'field' => 'slug',
                'terms' => $attrs['location'],
            ),
        ),

    );
    $jobs_by_location = new WP_Query($args);
    if ($jobs_by_location->have_posts()) :
        $location = str_replace('-', ' ', $attrs['location']);
        $display_by_location = '<div id="jobs-by-location">';
        $display_by_location .= '<h4>' . esc_html__($attrs['title']) . '&nbsp' . esc_html__(ucwords($location)) . '</h4>';
        $display_by_location .= '<ul>';
        while ($jobs_by_location->have_posts()) : $jobs_by_location->the_post();
            global $post;
            $deadline = get_post_meta(get_the_ID(), 'txt_application_deadline', true);
            $title = get_the_title();
            $slug = get_permalink();
            $display_by_location .= '<li class="job-listing">';
            $display_by_location .= sprintf('<a href="%s">%s</a>&nbsp&nbsp', esc_url($slug), esc_html__($title));
            $display_by_location .= '<span>' . esc_html($deadline) . '</span>';
            $display_by_location .= '</li>';
        endwhile;
        $display_by_location .= '</ul>';
        $display_by_location .= '</div>';
    else:
        $display_by_location = sprintf(__('<p class="job-error">Sorry, no jobs listed in %s where found.</p>'), esc_html__(ucwords(str_replace('-', ' ', $attrs['location']))));
    endif;

    wp_reset_postdata();

    if ($jobs_by_location->max_num_pages > 1 && is_page()) {
        $display_by_location .= '<nav class="prev-next-posts">';
        $display_by_location .= '<div call="nav-pervious">';
        $display_by_location .= get_next_posts_link(__('<span class="meta-nav">&larr;</span> Previous'), $jobs_by_location->max_num_pages);
        $display_by_location .= '</div';
        $display_by_location .= '<div class="next-posts-link">';
        $display_by_location .= get_previous_posts_link(__('<span class="meta-nav">&rarr;</span> Next'));
        $display_by_location .= '</div>';
        $display_by_location .= '</nav>';
    }

    return $display_by_location;

}

add_shortcode('shortcode_job_by_location', 'tamnc_shortcode_job_by_location');
