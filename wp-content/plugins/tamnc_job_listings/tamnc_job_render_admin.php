<?php
function tamnc_add_sub_page()
{
    add_submenu_page(
        'edit.php?post_type=job',
        __('Reorder jobs'),
        __('Reorder jobs'),
        'manage_options',
        'reorder-jobs-page',
        'tamnc_admin_reorder_jobs_callback');
}

add_action('admin_menu', 'tamnc_add_sub_page');

function tamnc_admin_reorder_jobs_callback()
{
    $args = array(
        'post_type' => 'job',
        'author_name' => 'admin',
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'post_status' => 'publish',
        //'no_found_rows' => false, // counts posts, remove if pagination required
        'post_per_page' => 1,
        'update_post_term_cache' => false, // grabs terms, remove if terms required (category, tag...)
        'update_post_meta_cache' => false, // grabs post meta, remove if post meta required
    );

    $jobs_list = new WP_Query($args);

    ?>
    <div id="job-sort" class="wrap">
        <div id="icon-job-admin" class="icon32"><br/></div>
        <h2>
            <?php _e('Sort Job Positions', 'wp-job-listing'); ?>
            <img src="<?php print esc_url(admin_url() . '/images/loading.gif'); ?>" alt="Loading"
                 class="loading-animation">
        </h2>
    </div>
    <?php if ($jobs_list->have_posts()) : ?>
        <p><?php _e('<strong>Note:</strong> Donec id est odio. Morbi tristique volutpat luctus'); ?></p>
        <ul class="custom-type-list">
            <?php while ($jobs_list->have_posts()) : $jobs_list->the_post(); ?>
                <li id="<?php esc_attr(the_id()); ?>"><?php esc_html(the_title()); ?></li>
            <?php endwhile; ?>
        </ul>
    <?php endif; ?>
    <?php
}

function tamnc_ajax_save_job_sort_order()
{
    if (!check_ajax_referer('job_reorder_nonce', 'security')) {
        wp_send_json_error(__('Invalid Nonce.'));
    }
    if (!current_user_can('manage_options')) {
        wp_send_json_error(__('You are not allow to do this.'));
    }

    $job_order = $_POST['order'];
    $counter = 0;

    foreach ($job_order as $key => $value) {
        $post = array(
            'ID' => $value,
            'menu_order' => $counter,
        );
        wp_update_post($post);
        $counter++;
    }
    wp_send_json_success();
}

add_action('wp_ajax_save_job_sort_order', 'tamnc_ajax_save_job_sort_order');