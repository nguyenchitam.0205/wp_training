<?php
/*
Plugin Name: Custom Default Menu
Description: A plugin to custom default menu
Version: 0.1
*/

class custom_default_menu {

	/*--------------------------------------------*
	 * Constructor
	 *--------------------------------------------*/

	/**
	 * Initializes the plugin by setting localization, filters, and administration functions.
	 */
	function __construct() {
		// add custom menu fields to menu
		add_filter( 'wp_setup_nav_menu_item', array( $this, 'cdm_add_custom_nav_fields' ) );

		// save menu custom fields
		add_action( 'wp_update_nav_menu_item', array( $this, 'cdm_update_custom_nav_fields'), 10, 3 );

		// edit menu walker
		add_filter( 'wp_edit_nav_menu_walker', array( $this, 'cdm_edit_walker'), 10, 2 );
	}

	/**
	 * Add custom fields to $item nav object
	 * in order to be used in custom Walker
	 *
	 * @access      public
	 * @since       1.0 
	 * @return      void
	*/
	function cdm_add_custom_nav_fields( $menu_item ) {

	    $menu_item->class = get_post_meta( $menu_item->ID, '_menu_item_class', true );
	    return $menu_item;

	}

	/**
	 * Save menu custom fields
	 *
	 * @access      public
	 * @since       1.0 
	 * @return      void
	*/
	function cdm_update_custom_nav_fields( $menu_id, $menu_item_db_id, $args ) {
	
	    // Check if element is properly sent
	    if ( isset($_REQUEST['menu-item-class']) AND is_array( $_REQUEST['menu-item-class']) ) {
	        $class_value = $_REQUEST['menu-item-class'][$menu_item_db_id];
	        update_post_meta( $menu_item_db_id, '_menu_item_class', $class_value );
	    }
	    
	}

	/**
	 * Define new Walker edit
	 *
	 * @access      public
	 * @since       1.0 
	 * @return      void
	*/
	function cdm_edit_walker($walker,$menu_id) {

	    return 'Walker_Nav_Menu_Edit_Custom';

	}

}

// instantiate plugin's class
$GLOBALS['custom_default_menu'] = new custom_default_menu();

include_once( 'edit_custom_walker.php' );
include_once( 'custom_walker.php' );